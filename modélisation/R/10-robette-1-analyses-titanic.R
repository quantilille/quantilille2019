library(dplyr)  # pour le "pipe" (%>%)
library(Hmisc)  # pour la discretisation de variable (fonction 'cut2')
library(FactoMineR)  # ACM, ACP, etc.
library(GGally)  # pour les graphiques bivaries et de regression (fonctions 'ggpairs' et 'ggcoef')
library(descr)  # pour les pseudo-R2 (fonction 'LogRegR2')
library(car)  # pour les diagnostics (fonctions 'outlierTest' et 'vif')
library(gvlma)  # pour les diagnostics des regressions lineaires
library(GDAtools)  # pour la dichotomisation de variables (fonction 'dichotom')

library(gam)  # Generalized Additive Models
library(glmnet)  # Lasso, ridge, elastic net
library(plsRglm)  # PLS1

library(rpart)  # arbres CART
library(rpart.plot)  # representation graphique des arbres
library(randomForest)  # random forests (algorithme de Breiman)
library(party)  # random forests (conditional inference)
library(randomForestSRC)  # dependances partielles et interactions
library(pdp)  # graphiques de dependances partielles
library(plotmo)  # graphiques de dependances partielles
library(pROC)  # calcul AUC

# Fonction pour le calcul de performance des modeles
perfs <- function(pred,actual) {
  t <- table(predicted=factor(as.numeric(pred>0.5),levels=c(0,1)),actual)
  acc <- sum(diag(t))/sum(t)
  tpr <- t[2,2]/sum(t[,2])
  tnr <- t[1,1]/sum(t[,1])
  bac <- (tpr+tnr)/2
  auc <- pROC::auc(actual,pred, quite=TRUE)
  res <- c(auc,acc,tpr,tnr,bac)
  res <- round(res,3)
  names(res) <- c('auc','acc','tpr','tnr','bac')
  return(res)
}

titanic <- read.csv('data/titanic.csv', stringsAsFactors=FALSE)
str(titanic)






## =========
## RECODAGES
## =========

base <- titanic[,c('Survived','Pclass','Sex','Age','Embarked')]
str(base)

base$Pclass[base$Pclass==1] <- '1st'
base$Pclass[base$Pclass==2] <- '2nd'
base$Pclass[base$Pclass==3] <- '3rd'
base$Pclass <- factor(base$Pclass)

base$Sex <- factor(base$Sex)

base$Embarked[base$Embarked==''] <- NA
base$Embarked[base$Embarked=='C'] <- 'Cherbourg'
base$Embarked[base$Embarked=='Q'] <- 'Queenstown'
base$Embarked[base$Embarked=='S'] <- 'Southampton'
base$Embarked <- factor(base$Embarked)

#base$CatAge <- cut2(base$Age,cuts=c(0,17,60))

base$Survived <- factor(base$Survived)

actual <- base$Survived






## =========================
## STATISTIQUES DESCRIPTIVES
## =========================

summary(base)
round(prop.table(table(base$Survived))*100, 1)






## ============
## TRIS CROISES
## ============

attach(base)
round(prop.table(table(Survived,Pclass),2)*100,1)
round(prop.table(table(Survived,Sex),2)*100,1)
round(prop.table(table(Survived,Embarked),2)*100,1)
boxplot(Age~Survived,notch=TRUE)
detach(base)

ggpairs(base, diag=list(continuous="density", discrete="bar"),
              upper=list(discrete='facetbar', combo='box'),
              lower=list(discrete='blank', combo='blank', continuous='blank'),
              axisLabels="show")






## ==========================================
## ANALYSE DES CORRESPONDANCES MULTIPLES (ACM)
## ==========================================

# Discretisation de Age
baseACM <- base
baseACM$Age <- cut2(baseACM$Age,cuts=c(0,17,60))

# ACM
acm <- MCA(baseACM,quali.sup=1,na.method='Average',graph=FALSE)

# Interpretation: graphique et contributions
plot(acm,choix='ind',invisible=c('var','quali.sup'))
plot(acm,choix='ind',invisible='ind')
round(acm$var$contrib[,1:2],1)






## =====================
## BASELINE PERFORMANCES
## =====================

# Prediction a la majorite
t <- table(actual)
pred.majo <- rep(names(which.max(t)), length(actual)) %>% as.character %>% as.numeric
perfs(pred.majo,actual)

# Prediction aleatoire
foo1 <- function(x) base::sample(c(0,1),length(actual),TRUE) %>% perfs(actual)
res1 <- lapply(1:1000, foo1) %>% do.call('rbind.data.frame',.) %>% colMeans
names(res1) <- c('auc','acc','tpr','tnr','bac')
res1 %>% round(3)

# Prediction aleatoire en respectant distribution marginale
probs <- table(actual)/length(actual)
foo2 <- function(x) base::sample(c(0,1),length(actual),TRUE,probs) %>% perfs(actual)
res2 <- lapply(1:1000, foo2) %>% do.call('rbind.data.frame',.) %>% colMeans
names(res2) <- c('auc','acc','tpr','tnr','bac')
res2 %>% round(3)






## =====================
## REGRESSION LOGISTIQUE
## =====================

# Estimation du modele
logit <- glm(Survived~.,base,family='binomial')
summary(logit)
round(exp(cbind(OR = coef(logit), confint(logit))),3)

# Representation graphique des resultats
ggcoef(logit, exponentiate=TRUE)

# Compute various pseudo-R2 measures:
LogRegR2(logit)

# Qualite de prediction
pred <- predict(logit, type="response", newdata=base)
(t <- table(actual,pred>0.5))
sum(diag(t))/sum(t)*100

# ou directement
perfs(pred,actual)

# NB: on aurait pu faire une regression lineaire
lm <- lm(as.numeric(as.character(Survived))~Pclass+Sex+Age+Embarked,base) 
summary(lm)
ggcoef(lm, exponentiate=FALSE)






## =======================================
## DIAGNOSTICS DE LA REGRESSION LOGISTIQUE
## =======================================

# Assessing Outliers
(out <- outlierTest(logit))  # Bonferonni p-value for most extreme obs

# Influential Observations
# Cook's D plot
# identify D values > 4/(n-k-1)
cutoff <- 4/((nrow(logit$model)-length(logit$coefficients)-2))
plot(logit, which=4, cook.levels=cutoff)

# Regression sans les influential
infl <- c(298,511,631)
logit.infl <- glm(Survived~., data=base[-infl,], family='binomial')
summary(logit.infl)
#summary(logit)

# Evaluate Collinearity
vif(logit) # variance inflation factors 
# pb si 3eme colonne > 2






## ==========================
## GENERALIZED ADDITIVE MODEL
## ==========================

# Tune degree
(tune <- sapply(1:10, function(x) gam(Survived ~ Pclass + Sex + 
                                        s(Age,x) + Embarked, 
                                      data=base, family=binomial)$aic))
which.min(tune) # pour savoir quel est le degré de polynome utile pour la variable quanti

# Estimation du modele
gam <- gam(Survived ~ Pclass + Sex + s(Age,5) + Embarked, data=base, family=binomial)
summary(gam)
summary.glm(gam)

# Qualite de prediction
pred <- predict(gam, type="response")
perfs(pred,actual)  # erreur car le modele supprime les observations avec une valeur manquante
perfs(pred,actual[complete.cases(base)])

# Graphiques
plotmo(gam, pmethod='partdep')






## =========================
## LASSO, RIDGE, ELASTIC NET
## =========================

param = 1  
# 1 -> LASSO
# 0 -> RIDGE
# 0.5 -> ELASTIC NET

# Estimation du modele
x_train <- model.matrix( ~ ., base[,-1])[,-1] # cv.glmnet demande à ce que l'on sépare les variable explicatives, et qu'elles soient sous cette forme particulier : matrice. Donc on discrétise les variables catégorielles sous forme 0/1
cv.lasso <- cv.glmnet(x=x_train, y=base[,1], 
                      family = "binomial", alpha=param, 
                      type.measure="deviance")  # erreur car le modele supprime les observations avec une valeur manquante
cv.lasso <- cv.glmnet(x=x_train, y=base[complete.cases(base),1], 
                      family = "binomial", alpha=param, 
                      type.measure="deviance")
coef(cv.lasso, s='lambda.1se') # plus exigeant dans la sélection des coefs
coef(cv.lasso, s='lambda.min') # moins exigeant

# Qualite de prediction
pred <- predict(cv.lasso,newx=x_train,type="response", s="lambda.min") %>% as.numeric
perfs(pred,actual[complete.cases(base)])

# Graphiques
lasso <- glmnet(x=x_train, y=base[complete.cases(base),1], family='binomial', alpha=param, lambda=cv.lasso$lambda.1se)
plotmo(lasso, pmethod='partdep')






## ====
## PLS1
## ====

# Estimation du modele

Y <- base$Survived
levels(Y) <- c('0','1')
Y <- as.numeric(as.character(Y))

X <- base[,-1]
facs <- sapply(X,is.factor)
X1 <- dichotom(X[,facs])
X2 <- data.frame(X[,!facs])
names(X2) <- names(X)[!facs]
X <- data.frame(X1,X2)

cv.modpls <- cv.plsRglm(dataY=Y, dataX=X, nt=10, modele="pls-glm-logistic", K=10)
res.cv.modpls <- cvtable(summary(cv.modpls, MClassed=TRUE))

res10 <- plsRglm(Y, X, nt=10, modele="pls-glm-logistic", pvals.expli=TRUE)
colSums(res10$pvalstep)

res <- plsRglm(Y, X, nt=3, modele="pls-glm-logistic", pvals.expli=TRUE)
res

# Qualite de prediction 
pred <- predict.plsRglmmodel(res, type='response')
perfs(pred,actual)






## ==============================
## CART (ARBRE DE CLASSIFICATION)
## ==============================

# Construction de l'arbre et resultats
rp <- rpart(Survived~.,base)
print(rp)
summary(rp)
rsq.rpart(rp)
plot(rp)
text(rp)

# Representation graphique
par(bg='white')
rpart.plot(rp,type=4,extra=104,faclen=0,fallen.leaves=FALSE,box.palette='auto')

# Parametre de complexite (gain de qualité du modèle par node ; plus le paramètre est faible, plus on est laxiste, et donc on continue à descendre dans l'arbre quand les gains sont faibles)
rp <- rpart(Survived~.,base,cp=0.001)
plotcp(rp)
rpart.plot(rp,type=4,extra=104,faclen=0,fallen.leaves=FALSE,box.palette='auto')

# Autres parametres : mini observations par feuille pour que l'on accepte de faire une segmentation supplémentaire.
rp <- rpart(Survived~.,base,minsplit=10)
rpart.plot(rp,type=4,extra=104,faclen=0,fallen.leaves=FALSE,box.palette='auto')

# Qualite de prediction
pred <- predict(rp,type='vector')-1
perfs(pred,actual)

# Importance des variables
rp$variable.importance

# NB: peut aussi servir d'aide a la discretisation d'une variable continue
rp <- rpart(Survived~Age,base,cp=0.0055)
print(rp)






## =============
## RANDOM FOREST
## =============

# Construction de la foret
rf <- randomForest(Survived~.,base,na.action=na.omit)
print(rf)

# Qualite de prediction
# Matrice de confusion et "accuracy"
(mc <- rf$confusion)
sum(diag(mc))/sum(mc)

# ou
pred <- predict(rf) %>% as.character %>% as.numeric
perfs(pred,actual[complete.cases(base)])

# Taux d'erreur selon le nombre d'arbres
plot(rf,main='error rate by ntree') #, ylim=c(0,0.36))
legend('topright', colnames(rf$err.rate), col=1:3, fill=1:3)

# Imputation de valeurs manquantes
baseRF <- rfImpute(Survived~.,data=base)
rf <- randomForest(Survived~.,baseRF)
pred <- predict(rf) %>% as.character %>% as.numeric
perfs(pred,actual)

# "Tuning" de mtry
tuneRF(baseRF[,-1],baseRF$Survived)
tuneRF(baseRF[,-1],baseRF$Survived,mtryStart=3)
rf <- randomForest(Survived~.,baseRF, mtry=2)
pred <- predict(rf) %>% as.character %>% as.numeric
perfs(pred,actual)

# Importance des variables
rf <- randomForest(Survived~.,baseRF,importance=TRUE)
rf$importance
varImpPlot(rf,scale=FALSE)

# Dependances partielles
rfsrc <- rfsrc(Survived~.,baseRF)
vars <- c('Sex','Pclass','Age','Embarked')
plot.variable(rfsrc,xvar.names=vars,target='1',partial=TRUE, ylim=c(0,1))
plot.variable(rfsrc,xvar.names='Age',target='1',partial=TRUE, ylim=c(0,1), plots.per.page=1)

# En plus joli
graphes <- plot.variable(rfsrc, xvar.names=vars, target='1', partial=TRUE, show.plots=FALSE)
dt <- graphes$pData[[1]]
y <- dt$yhat
x <- lapply(dt$x.uniq,function(x) rep(x,graphes$n)) %>% unlist
boxplot(y~x, col="dodgerblue3", notch=FALSE, ylim=c(0,1))

# Comparaison avec dependances marginales
plot.variable(rfsrc,xvar.names=vars,target='1',partial=FALSE)

# Interactions
find.interaction(rfsrc, method='vimp',nrep=1)
pdep <- partial(rf, c('Sex','Pclass'), which.class=2, prob=TRUE)
plotPartial(pdep)
pdep <- partial(rf, c('Sex','Age'), which.class=2, prob=TRUE)
plotPartial(pdep)
pdep <- partial(rf, c('Pclass','Age'), which.class=2, prob=TRUE)
plotPartial(pdep)
pdep <- partial(rf, c('Sex','Pclass','Age'), which.class=2, prob=TRUE)
plotPartial(pdep)

# Dependances partielles sous forme de tableau (i.e tableau des effets nets, "toutes choses égales par ailleurs")
foo <- function(fit,nomvar) {
  pdep <- partial(fit, nomvar, which.class=2, prob=TRUE, ice=TRUE, grid.resolution=10)
  foo1 <- function(x) (max(0,median(x,na.rm=T)-1.57*IQR(x,na.rm=T)/sqrt(length(x)))) %>% round(3)
  foo2 <- function(x) (median(x,na.rm=T)+1.57*IQR(x,na.rm=T)/sqrt(length(x))) %>% round(3)
  var <- quo(nomvar)
  res <- pdep %>% group_by(!!rlang::sym(nomvar)) %>% summarise(prob=median(yhat),
                                                             ICinf=foo1(yhat),
                                                             ICsup=foo2(yhat))
  return(as.data.frame(res))
}
vars <- c('Sex','Pclass','Age','Embarked')
tabres <- lapply(vars,function(x) foo(rf,x))
for(i in 1:length(tabres)) {
  names(tabres[[i]])[1]='X'
  if(is.numeric(tabres[[i]][,1])) tabres[[i]][,1] <- round(tabres[[i]][,1],1)
  tabres[[i]][,1] <- as.factor(tabres[[i]][,1])
}
tabres <- do.call('rbind.data.frame',tabres)
tabres






## ===============================
## CONDITIONAL INFERENCE FRAMEWORK
## ===============================
# Plus lent mais supposé ne pas être biaisé en faveur des variables continues ou des variables catégorielles comportant beaucoup de modalités.


# Tree
ct <- ctree(Survived~.,data=baseRF)
print(ct)
plot(ct,type='extended',inner_panel=node_inner(ct,id=FALSE),terminal_panel=node_terminal(ct,id=FALSE))

# Qualite de prediction de l'arbre
pred <- predict(ct) %>% as.character %>% as.numeric
perfs(pred,actual)

# Forest
cf <- cforest(Survived~.,data=baseRF,control=cforest_unbiased(mtry=2))

# Qualite de prediction de la foret
pred <- predict(cf) %>% as.character %>% as.numeric
perfs(pred,actual)

# Variable importance
(condimp <- varimp(cf, conditional=FALSE)) %>% sort
(condimp <- varimpAUC(cf, conditional=FALSE)) %>% sort

# Comparaison avec random forest de Breiman
rf <- randomForest(Survived~.,baseRF,importance=TRUE)
op <- par(mfrow=c(1,2))
dotchart(rf$importance[,3],main='mean decrease accuracy')
dotchart(condimp,main='unbiased')

# Graphiques de dependances partielles
vars <- names(baseRF)[-1]
par(mfrow=c(2,2),mar=c(1,1,1,1))
for(i in 1:length(vars)) {
  pdep <- partial(cf, vars[i], which.class=2, prob=TRUE)
  plot(pdep[,1],pdep[,2],type='l',ylim=c(0,1),main=vars[i])
}

# Dependances partielles sous forme numerique (et non graphique)
# ici pour la variable de classe
pdep_class <- partial(cf, 'Pclass', which.class=2, prob=TRUE, ice=TRUE) # a noter, l'option ice=TRUE
# probabilite mediane uniquement
pdep_class %>% 
  group_by(Pclass) %>%
  summarise(proba=round(median(yhat),3)) %>%
  as.data.frame
# min, q1, mediane, q3 et max
pdep_class %>% group_by(Pclass) %>% summarise(min=min(yhat),
                                            Q1=quantile(yhat,probs=0.25),
                                            median=median(yhat),
                                            q3=quantile(yhat,probs=0.75),
                                            max=max(yhat))






## ========================
## RECODAGES PLUS AMBITIEUX
## ========================

base <- titanic

# Relevel Pclass
base$Pclass[base$Pclass==1] <- '1st'
base$Pclass[base$Pclass==2] <- '2nd'
base$Pclass[base$Pclass==3] <- '3rd'

# Grab title from passenger names
base$Title <- gsub('(.*, )|(\\..*)', '', base$Name)
table(base$Sex, base$Title)
base$Title[base$Sex=='male' & base$Title %in% c('Capt','Col','Don','Dr','Jonkheer','Major','Sir')] <- 'Mr'
base$Title[base$Title %in% c('Mlle','Ms')] <- 'Miss'
base$Title[base$Sex=='female' & base$Title %in% c('Dr','Lady','Mme','the Countess')] <- 'Mrs'
table(base$Sex, base$Title)

# Grab surname from passenger name
base$Surname <- sapply(base$Name, function(x) strsplit(x, split = '[,.]')[[1]][1])

# Create a family size variable including the passenger themselves
base$Fsize <- base$SibSp + base$Parch + 1

# Create a family variable 
base$Family <- paste(base$Surname, base$Fsize, sep='_')

# Discretize family size
base$FsizeD[base$Fsize == 1] <- 'singleton'
base$FsizeD[base$Fsize < 5 & base$Fsize > 1] <- 'small'
base$FsizeD[base$Fsize > 4] <- 'large'

# Since their fare was $80 for 1st class, they most likely embarked from 'C'
base$Embarked[c(62,830)] <- 'C'
base$Embarked[base$Embarked=='C'] <- 'Cherbourg'
base$Embarked[base$Embarked=='Q'] <- 'Queenstown'
base$Embarked[base$Embarked=='S'] <- 'Southampton'

# Make variables factors into factors
factor_vars <- c('Survived','Pclass','Sex','Embarked','Title','Surname','Family','FsizeD')
base[factor_vars] <- lapply(base[factor_vars], function(x) as.factor(x))

# We make a prediction of a passengers Age using the other variables and a random forest model. 
base$AgeImp <- base$Age
rf_age <- randomForest(AgeImp ~ Pclass + Sex + Fare + Embarked + Title + Fsize + SibSp + Parch,
                       data=base[!is.na(base$AgeImp),])
plot(x=rf_age$y,y=predict(rf_age),xlab='actual',ylab='predicted',xlim=c(0,80),ylim=c(0,80))
abline(a=0,b=1,col='grey')
mean(rf_age$rsq)
base$AgeImp[is.na(base$AgeImp)] <- predict(rf_age, base[is.na(base$AgeImp),])
sum(is.na(base$AgeImp))

# Discretize AgeImp
base$CatAge <- cut2(base$AgeImp,cuts=c(0,17,60))

# Save database
TitaBase <- base[,c('Survived','Pclass','Sex','Fare','Embarked','Title','FsizeD','AgeImp')]
#write.csv2(TitaBase,'TitaBase.csv',row.names=FALSE)
str(TitaBase)