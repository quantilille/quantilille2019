# Introduction a R et RStudio ----
# Manipuler et decrire des donnees simples
# Quantilille 2019 - Mardi 25 juin 2019
# Pierre Blavier et Anton Perdoncin

# 1. PRESENTATION DU LANGAGE ----

# R est d'abord une grosse calculette ----
1 + 3

450 / 78

exp(-2)

# Un langage oriente objets ----

x <- -8

y <- 9

x+8

truc <- x + y

truc

# Exemple a partir d'un vecteur numerique ----

tailles <- c(167, 180, 192, 173, 174, 172, 167, 171, 
             205, 185, 163, 170)

length(tailles)   # Nombre d'elements

mean(tailles)     # Moyenne
sum(tailles) / length(tailles) # une autre façon de calculer la moyenne

var(tailles)      # Variance

min(tailles)      # Minimum

max(tailles)      # Maximum

sd(tailles)       # ecart-type
sqrt(sum((tailles - mean(tailles))^2)/(length(tailles) - 1)) # une autre façon de calculer l'ecart-type



## Arguments d'une fonction ----

tailles2 <- c(167, NA, 192, 173, 174, 172, 167, 171, 
              NA, 185, 163, 170)

mean(tailles2)

mean(tailles2, na.rm = TRUE)

# Appeler a l'aide ----

help("mean")

?mean


# 2. CHARGER DES PACKAGES ----
library(tidyverse)
library(questionr)
# C'est generalement la premiere chose que vous faites en haut d'un script.


##
dataSubPath <- 'data/synth'


# 3. IMPORTER UN JEU DE DONNEES ---- 

d <- read_csv2(
  ##"data/ee2013_extraits_tdintro.csv"
  file.path( dataSubPath , 'ee2013_extraits_tdintro.csv')
)

# Il s'agit d'un echantillon (aleatoire simple) de l'EEC 2003, et d'un selection d'un petit nombre de variables.

# 4. INSPECTER UN JEU DE DONNEES ---- 

names(d) # quelles sont les variables du jeu de donnees ?

summary(d) # resume rapide des variables

View(d) # visualiser le jeu de donnees dans un onglet separe



# 5. RECODER UNE VARIABLE NUMERIQUE ----

class(d$age)

table(is.na(d$age)) # y a-t-il des NA ?

summary(d$age)

# Choisir soi-meme les bornes des classes : cut ----

d$age2 <- cut(d$age, 
              breaks = c(15, 25, 35, 45, 55, 65,
                         75, 85, 94))

freq(d$age2) # attention ! 296 NA sont apparus !

# C'est un probleme recurrent : lorsque R ne sait pas que faire de certaines cases, il les place en NA. Il faut toujours verifier apres avoir recode une variable !

d$age2 <- cut(d$age,
              breaks = c(15, 25, 35, 45, 55,
                         65, 75, 85, 95),
              right = TRUE, # argument qui permet d'inclure la borne superieure
              include.lowest=TRUE) # argument qui permet d'inclure la borne inf

freq(d$age2) # ok

# Creer des quantiles : quant.cut ----

d$age_quartiles <- quant.cut(d$age, 4)

freq(d$age_quartiles)


d$age_quintiles <- quant.cut(d$age, 5)

freq(d$age_quintiles)

# ad libitum... on peut creer des deciles, des centiles, ou tout autre quantile


# Renommer les modalites de la variable numerique decoupee en classes : cut(... labels = ) ----

d$age3 <- cut(d$age, 
              breaks = c(15, 25, 45, 65, 85, 95), 
              include.lowest=TRUE, 
              labels = c("<25", "25-44", "45-64", "65-84", ">85"))

freq(d$age3)

freq(d$age3, exclude = NA, total = TRUE)


# 6. RECODER UNE VARIABLE CATEGORIELLE ----

# Renommer des modalites ----

class(d$immi) 

summary(d$immi) # absurde...

freq(d$immi)

d$immigration <- fct_recode(factor(d$immi), 
                            "Immigres" = "1",
                            "Non immigres" = "2")

# Pour que fct_recode marche, il faut que la variable soit un "factor"

freq(d$immigration)

# Se debarrasser d'une modalite genante : na_if et droplevels ----
d <- d %>% 
  mutate(cstotr = as.factor(cstotr)) %>% 
  mutate(gsp = fct_recode(cstotr,
                          "Agriculteurs exploitants" = "1",
                          "Artisans, commerçants, chefs d'entreprise" = "2",
                          "Cadres et professions intellectuelles superieures" = "3",
                          "Professions intermediaires" = "4",
                          "Employes" = "5",
                          "Ouvriers" = "6",
                          "Inactifs ayant deja travaille" = "7",
                          "Autres inactifs" = "8"))

freq(d$gsp)

# deux problemes : la valeur 0 correspond a des individus n'ayant pas repondu, donc plutôt des NA ; 

d <- d %>% 
  mutate(cstotr = as.factor(cstotr)) %>% 
  mutate(gsp = fct_recode(cstotr,
                          "Agriculteurs exploitants" = "1",
                          "Artisans, commerçants, chefs d'entreprise" = "2",
                          "Cadres et professions intellectuelles superieures" = "3",
                          "Professions intermediaires" = "4",
                          "Employes" = "5",
                          "Ouvriers" = "6",
                          "Inactifs ayant deja travaille" = "7",
                          "Autres inactifs" = "8")) %>% 
  mutate(gsp = na_if(gsp, "0")) %>% # pour indiquer que la valeur 0 devient NA
  mutate(gsp = droplevels(gsp)) # pour que 0 n'apparaisse plus dans la liste des modalites

freq(d$gsp) # ok

freq(d$gsp, exclude = NA)


# Rassembler une liste de modalites : fct_collapse ----
d <- d %>% 
  mutate(gsp2 = fct_collapse(gsp, 
                             "Classes pop" = c("Employes", "Ouvriers"),
                             "Inactifs" = c("Inactifs ayant deja travaille", "Autres inactifs")))

d %>% 
  select(cstotr, gsp, gsp2) %>% 
  slice(1:20)


freq(d$gsp2)


# Combiner des variables : if_else et case_when ----

d <- d %>% 
  mutate(mig_sexe = if_else(sexe == 1 & immi == 1, # condition
                            "Hommes immigres", # modalite si la condition est vraie
                            "Autres")) # modalite sinon

freq(d$mig_sexe)


# case_when est une generalisation de if_else

d <- d %>% 
  mutate(mig_sexe2 = case_when(sexe == 1 & immi == 1 ~ "Immigres",
                               sexe == 2 & immi == 1 ~ "Immigrees",
                               TRUE ~ "Non immigre.e.s")) # modalite pour les autres combinaisons possibles

freq(d$mig_sexe2)


# 8. SELECTIONNER DES INDIVIDUS OU DES VARIABLES ----

# Creer une sous-population : filter ----

immig <- d %>% 
  filter(immi == 1)

non_immi <- d %>% 
  filter(!immi == 1) # ! exprime la negation d'une condition ; on obtiendrait le meme resultat avec immi == 2

immig_h <- d %>% 
  filter(sexe == 1 & immi == 1) # toutes les combinaisons sont possibles...


# Selectionner des variables : select ----

d2 <- d %>% 
  select(immi, sexe)


d3 <- d  %>% 
  select(starts_with("immi")) # on peut selectionner des variables en fonction de leur nom (marche aussi avec ends_with ou contains)

toto <- d %>% 
  select(1:2, immi, acteu)

titi <- d %>% 
  select(age:ddipl)


# 8. EXERCICES ----

# 1/ Decoupez la variable "salred" (salaire mensuel). Soyez attentifs a la distribution de la variable... et aux NA 

summary(d$salred) # permet d'observer des indicateurs de la distribution 
# Attention, il faut remarquer qu'elle comporte plein de valeurs manquantes ... 

d$salaire_decoupe <- cut(d$salred, 
                         breaks = c(0, 1000, 1500, 2000, 13000), 
                         include.lowest=TRUE, 
                         labels = c("<1000", "1000-1500", "1500-2000", ">2000"))
# attention à bien gérer l'option "include.lowest" qui détermine la prise en compte ou non de la valeur du bas (ici incluse)

freq(d$salaire_decoupe)

freq(d$salaire_decoupe, exclude = NA, total = TRUE)

# on aurait aussi pu la découper en quartiles : 

d$salaire_quartiles <- quant.cut(d$salred, 4)

freq(d$salaire_quartiles)


# 2/ Renommez les modalites de la variable sexe

d <- d %>% 
  mutate(sexe = as.factor(sexe)) %>% 
  mutate(sex = fct_recode(sexe, 
                          "Hommes" = "1",
                          "Femmes" = "2"))
freq(d$sex)

# 3/ Creez une nomenclature agregee de diplômes a partir de la variable ddipl


d <- d %>% 
  mutate(ddipl = as.factor(ddipl)) %>% 
  mutate(diplome = fct_recode(ddipl,
                              "Diplôme supérieur à baccalauréat + 2 ans" = "1",
                              "Baccalauréat + 2 ans" = "3",
                              "Baccalauréat ou brevet professionnel" = "4",
                              "CAP ou BEP" = "5",
                              "Brevet des collèges" = "6",
                              "Aucun diplôme ou certificat d'études primaires" = "7"))

freq(d$diplome)

# 4/ Combinez la variable sexe et la variable acteu

# Statut d'activité : variable acteu
freq(d$acteu)

d <- d %>% 
  mutate(acteu = as.factor(acteu)) %>% 
  mutate(statut_emploi = case_when(
    acteu  == "1" ~ "Occupe",
    acteu == "2" ~ "Chomeur",
    acteu == "3" ~ "Inactif"))

freq(d$statut_emploi)


# 5/ Creez un nouveau jeu de donnees ne contenant que des femmes ; creez un nouveau jeu de donnees dont les variables contiennent la chaîne de caracteres "dip".

# Creation d'un jeu de donnees avec seulement femmes 
femmes <- d %>% 
  filter(sex==c("Femmes"))  

freq(femmes$sex)

# Toutes les variables qui contiennent "dip" 
names(d)

d_dip <- d  %>% 
  select(contains("dip")) # on peut selectionner des variables en fonction de leur nom (marche aussi avec ends_with ou starts_with)

names(d_dip)
