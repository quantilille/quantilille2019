Les fichiers dans le sous–répertoire `data/synth`

- basesEHF.RData
- ee2013_extraits_tdintro.csv
- ee2013_extraits_tdreglog.csv
- FQP_2016_ad_hoc.dta
- REPONSE_sal_2011.dta
- REPONSE_sal_2011_ordo.dta

contiennent des données **de synthèse**, ces fichiers ne pouvant être archivés dans ce dépôt.  
De plus, les colonnes `nafg017n`, `region` et `secteur` ont été supprimées.
