## 26/06/2019 : TD5 - Expressions regulières
## ANF Données du web (Julien Boelaert, Etienne Ollion)

################################################################################
## Discographie de Miles Davis sur allmusic.com
## ("http://www.allmusic.com/artist/miles-davis-mn0000423829/discography")
##    0) accéder à la page et la parser
##    1) Extraire les informations d'album : année, nom, label
##    2) Les nettoyer
##    3) Les mettre dans une data.frame
##    4) Ne conserver que les albums du label Prestige

##########
##    0) accéder à la page et la parser
##########
library(RCurl)
library(XML)
page.brute <- getURL("http://www.allmusic.com/artist/miles-davis-mn0000423829/discography", 
                            followlocation= TRUE,
                            timeout = 60, 
                            useragent = "Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0")

page.parsee <- htmlParse(page.brute)


##########
##    1) Extraire les informations d'album : année, nom, label
##########
annee <- xpathSApply(page.parsee, "//td[@class='year']", xmlValue)
titre <- xpathSApply(page.parsee, "//td[@class='title']", xmlValue)
label <- xpathSApply(page.parsee, "//td[@class='label']", xmlValue)

## Ou encore directement extraire le tableau
tableau <- readHTMLTable(page.brute)
tableau <- tableau[[1]]

##########
##    2) Les nettoyer
##########
annee <- gsub("\\n", "", annee)
annee <- gsub(" ", "", annee)

titre <- gsub("\\n", "", titre)
titre <- gsub("^\\s+|\\s+$", "", titre)

label <- gsub("\\n", "", label)
label <- gsub("^\\s+|\\s+$", "", label)


##########
##    3) Les mettre dans un data.frame
##########
miles <- data.frame(annee, titre, label)

##########
##    4) Ne conserver que les albums du label Prestige
##########
miles <- miles[grep("Prestige", miles$label), ]

