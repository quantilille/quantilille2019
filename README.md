# Dépôt de l'école d'été Quantilille 2019

Ce dépôt rassemble les présentations, scripts, données et autres ressources utilisées lors de [l’École d'été méthodes quantitatives en sciences sociales](https://ceraps.univ-lille.fr/quantilille/) 2019.

Nous remercions les organisateurs de nous avoir permis de mettre leurs supports de formation en ligne.

## Licences

Les contenus des cours sont diffusés sous licence Creative Commons [Attribution-ShareAlike](https://creativecommons.org/licenses/by-sa/4.0/) (BY-SA) sauf mentions contraires dans les sous&ndash;répertoires. Vous êtes libre d'utiliser ces contenus et de les modifier à la seule condition d'en accréditer la provenance et de les rediffuser selon les mêmes termes.

## Module Principes et usages de la modélisation

Module organisé par [Pierre Blavier](https://pro.univ-lille.fr/pierre-blavier) et [Anton Perdoncin](https://lubartworld.cnrs.fr/anton-perdoncin-fr/)  
[programme](modélisation/0_quantilille2019_programme.pdf)  
[présentations](modélisation/présentations)  
[scripts R](modélisation/R)

### Lundi 24 juin (matin)
* L’accès aux données de la statistique publique : présentation de la PUDL.

### Lundi 24 juin (après-midi)
* Comment et pourquoi modéliser ;
* Le cadre inférentiel et les tests d’inférence 
* Organiser le travail de quantification

### Mardi 25 juin (matin)
* Introduction à R et RStudio 
* Recoder des variables numériques et catégorielles 
* Tris à plat et tris croisés.

### Mardi 25 (après-midi)
* Modélisation linéaire : théorie et exemples 
* Représenter des variables numériques avec R 
* Réaliser et représenter les résultats d’une régression linéaire

### Mercredi 26 juin (matin)
* Modélisation logistique : théorie et exemples 
* Représenter graphiquement des variables catégorielles 

### Mercredi (après-midi)
* Réaliser des régressions logistiques 
* Exporter et représenter des résultats de régressions logistiques

### Jeudi 27 juin (matin)
* Bons et mauvais usages des odds ratios 
* Lire et comparer des modèles 
* Transformer les coefficients en probabilité : principes et mise en oeuvre

### Jeudi 27 (après-midi)
* Modélisation polytomique : théorie et exemples ;
* Réaliser et représenter les résultats d’une régression polytomique

### Vendredi 28 juin (Nicolas Robette) (matin)
* Les alternatives à la modélisation : principes et exemples

### Vendredi 28 juin (Nicolas Robette) (après-midi)
* Exemples de mise en oeuvre avec R

**Note :** 

Les fichiers dans le sous–répertoire `data/synth` contiennent des données **de synthèse**, ces fichiers ne pouvant être archivés dans ce dépôt.

## ANF Données du web

ANF organisée par [Étienne Ollion](https://ollion.cnrs.fr/) et [Julien Boelaert](https://pro.univ-lille.fr/julien-boelaert)  
[programme](données-du-web/Programme_et_intervenants_de_la_formation_-_ANF__-_DONNEES_DU_WEB_-__24-27_JUIN_2019.pdf)  
[présentations](données-du-web/présentations)  
[scripts R](données-du-web/R)

### Lundi 24 juin (matin)

* Stratégies numériques pour les sciences sociales (Étienne Ollion)

### Lundi 24 juin (après-midi)

* Introduction à R (Julien Boelaert)

### Mardi 25 juin (matin)

* Comment s’écrit le web ? Comment le lire ? (Étienne Ollion)

### Mardi 25 (après-midi)

* Sélectionner des informations : le langage XPath (Julien Boelaert)  

### Mercredi 26 juin (matin)

* Sélectionner des informations : le langage XPath (Julien Boelaert)

### Mercredi (après-midi)

* Curation de données : expressions régulières, open refine (Julien Boelaert)

### Jeudi 27 juin (matin)

* Enjeux légaux et éthiques de la récolte de données (Thomas Soubiran)

### Jeudi 27 (après-midi)

* Automatisation de la collecte (Julien Boelaert)
